var request = require('superagent'),
    extend = require('extend'),
    async = require('async');

function doRequest(cmd, params, next) {
    params = extend(params, {
        format: 'json'
    });

    request
        .get('http://api.steampowered.com/'+ cmd +'/v0001/')
        .query(params)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .end(function (err, res) {
            next(err ? (res && res.body ? res.body : err)
                : null, res ? res.body : null
            );
        });
}

module.exports = {
    checkAccess: function(apiKey, appId, steamId, next) {
        doRequest('ISteamUser/CheckAppOwnership', {key: apiKey, steamid: steamId, appid: appId}, function(err, result) {
            next(err, result && result.appownership ? result.appownership.ownsapp : false)
        });
    },
    authenticateUserTicket: function(apiKey, appId, ticket, next) {
        doRequest('ISteamUserAuth/AuthenticateUserTicket', {key: apiKey, appid: appId, ticket: ticket}, function(err, result) {
            next(err, result && result.response && result.response.params && result.response.params.steamid ? result.response.params.steamid : false)
        });
    },
    getPlayerSummaries: function(apiKey, steamId, next) {
        doRequest('ISteamUser/GetPlayerSummaries', {key: apiKey, steamids: steamId}, function(err, result) {
            next(err, result && result.response && result.response.players.player ? result.response.players.player : false)
        });
    }
};