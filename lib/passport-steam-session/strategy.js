/**
 * Module dependencies.
 */
var passport = require('passport'),
    SteamWebAPI = require('./steam-api'),
    util = require('util');



/**
 * Retrieve user's Steam profile information.
 *
 * @param  {String} key     Steam WebAPI key.
 * @param  {String} steamID SteamID64.
 * @param  {function} callback
 * @return {Object}         User's Steam profile.
 */
function getUserProfile(key, steamID, callback) {
    SteamWebAPI.getPlayerSummaries(
        key,
        steamID,
        function(err, players) {
            if(err || !players) {
                return callback(err);
            }

            var profile = {
                provider: 'steam',
                _json: players[0],
                id: players[0].steamid,
                displayName: players[0].personaname,
                photos: [{
                    value: players[0].avatar
                }, {
                    value: players[0].avatarmedium
                }, {
                    value: players[0].avatarfull
                }]
            };

            callback(null, profile);
        }
    );
}



/**
 * `Strategy` constructor.
 *
 * The steam session authentication strategy authenticates requests based on the session header or param.
 */
function Strategy(options, verify) {
    if (typeof options == 'function') {
        verify = options;
        options = {};
    } else {
        this.options = options;
    }

    if (!options || !options.apiKey || !options.appId) {
        throw new Error('You must provide apiKey and appId configuration value to use passport-steam-session.');
    }

    if (!verify) throw new Error('local authentication strategy requires a verify function');

    this._steamSessionKeyField = options.apiKeyField || 'steamTicket';
    this._steamSessionHeader = options.apiKeyHeader || 'steamTicket';

    passport.Strategy.call(this);
    this.name = 'steam-session';
    this._verify = verify;
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a form submission.
 *
 * @param {Object} req
 * @param {Object} options
 * @api protected
 */
Strategy.prototype.authenticate = function(req, options) {
    options = options || {};
    options.profile = (options.profile === undefined) ? (
        this.options.profile  === undefined ? true : this.options.profile
        ) : options.profile;

    options.passReqToCallback = (options.passReqToCallback === undefined) ? (
            this.options.passReqToCallback  === undefined ? false : this.options.passReqToCallback
        ) : options.passReqToCallback;

    var ticket = lookup(req.body, this._steamSessionKeyField)
        || lookup(req.query, this._steamSessionKeyField)
        || lookup(req.params, this._steamSessionKeyField)
        || lookup(req.headers, this._steamSessionHeader);

    if (!ticket) {
        return this.fail({ message: 'Steam ticket is invalid.' });
    }
    //TODO: Check session, resolve steamId

    var self = this;

    SteamWebAPI.authenticateUserTicket(self.options.apiKey, self.options.appId, ticket, function(err, steamID){
        if (steamID) {
            if(options.profile) {
                getUserProfile(self.options.apiKey, steamID, function(err, profile) {
                    if(err) {
                        self.error(err);
                    } else if (options.passReqToCallback) {
                        self._verify(req, steamID, profile, verified);
                    } else {
                        self._verify(steamID, profile, verified);
                    }
                });
            } else {
                if (options.passReqToCallback) {
                    self._verify(req, steamID, verified);
                } else {
                    self._verify(steamID, verified);
                }
            }
        } else {
            return self.fail({ message: 'Steam ticket is invalid.' });
        }
    });

    function verified(err, user, info) {
        if (err) { return self.error(err); }
        if (!user) { return self.fail(info); }
        self.success(user, info);
    }
};


function lookup(obj, field) {
    if (!obj) { return null; }
    var chain = field.split(']').join('').split('[');
    for (var i = 0, len = chain.length; i < len; i++) {
        var prop = obj[chain[i]];
        if (typeof(prop) === 'undefined') { return null; }
        if (typeof(prop) !== 'object') { return prop; }
        obj = prop;
    }
    return null;
}

/**
 * Expose `Strategy`.
 */
module.exports = Strategy;